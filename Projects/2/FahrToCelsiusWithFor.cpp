#include <stdio.h>
/* вывод таблицы температур по Фаренгейту и Цельсию
   для fahr */

int main(int argc, char *argv[])
{
    int fahr;

    for (fahr = 500; fahr >= 0; fahr = fahr - 10)
    {
        printf("%3d %6.1f\n", fahr, (5.0/9.0)*(fahr-32));
    }
    
    return 0;
}
