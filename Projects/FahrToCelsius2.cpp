#include <stdio.h>
/* вывод таблицы температур по Фаренгейту и Цельсию
   для fahr = 0, 20, ..., 300 */

int main(int argc, char *argv[])
{
    const int lower = 0;   /*нижняя граница температур*/
    const int upper = 300; /*верхняя граница температур*/
    const int step = 20;   /*величина шага*/

    float fahr = lower;
    while (fahr <= upper) {
        const float celsius = (5.0/9.0) * (fahr-32.0);
        
        printf("fahr: %3.0f cels: %6.2f\n", fahr, celsius);
        
        fahr = fahr + step;
    }
    
    return 0;
}
