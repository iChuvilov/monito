#include <stdio.h>
/* вывод таблицы температур по Фаренгейту и Цельсию
   для fahr */

int main(int argc, char *argv[])
{
    for (int fahr = 500; fahr >= 0; fahr = fahr - 10)
    {
        const int cels = (5.0 / 9.0) * (fahr - 32);
        printf("%3d %6.1f\n", fahr, cels);
    }
    
    return 0;
}
